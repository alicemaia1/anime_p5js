var dinos = [];
var N = 10;
var cont = 0;
var cont_frames = 0;
var delay = 10;

function setup() {
    createCanvas(1350, 635);
    for(let i = 1; i <= N; i++) {
        dinos.push(loadImage("imgs/Walk ("+i+").png"))
    }
  }
  
function draw() {
    cont_frames++;
    if (!(cont_frames % delay)) {
    background('#fae');

    let img = dinos[cont];
    image(img,10+cont_frames,10,img.width/4,img.height/4);

    cont++;
    if(cont >= N) {
        cont = 0;
    }
    if(cont_frames >= 1350) {
        cont_frames = 0;
    }
    }
  }